from crypto.app import App
from crypto.market_events.event import MockEvent
from crypto.message_delivery.sender import Mocksender


senders = [Mocksender()]
events = [MockEvent(senders)]
app = App (events)
app.start()
