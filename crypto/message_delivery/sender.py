from abc import ABC, abstractmethod
class Sender(ABC):
    @abstractmethod
    def send(self,message):
        pass
class Mocksender(Sender):
    def send(self,message):
        print("Sent using mock sender: {message}")